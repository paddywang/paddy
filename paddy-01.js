// PaddyWang

// paddy 里面所有的处理对象都是按照数组来处理的
// 并对IE进行了兼容性处理
// 
// 增 ：
//  > paddy(html)  html字符串
//  > appendTo 支持 选择器 DOM对象 DOM对象数组 paddy对象
// 查 ：
//  > paddy(selector) 基本选择器
// 静态方法：
//  > each  trim  is...

var paddy = 
(function(){
function paddy(str){
	return new paddy.fn.init(str);
}
// 原型
paddy.fn = paddy.prototype = {
	constructor: paddy,
	str: null,
	init: function(str){
		// 字符串
		if(typeof str === 'string'){
			if(str.charAt(0) === '<'){
				// html
				this.elements = parseHTML(str);
			}else {
				// selector
				this.elements = select(str);
			}
		// DOM对象
		}else if(paddy.isDOM(str)){
			this.elements = [str];
		}else if(paddy.isPaddy(str)){
			this.elements = str.elements;
		}else if(paddy.isLikeArray(str)){
			this.elements = str;
		}
		this.str = 'paddy';
	},
	extend: function(obj){
		var k;
		for(k in obj){
			this[k] = obj[k];
		}
	}
};
paddy.fn.init.prototype = paddy.fn;
paddy.extend = paddy.fn.extend;
// 判断函数
paddy.extend({
	isString: function(o){
		return typeof o === 'string';
	},
	isNumber: function(o){
		return typeof o === 'number';
	},
	isBoolean: function(o){
		return typeof o === 'boolean';
	},
	isFunction: function(o){
		return typeof o === 'function';
	},
	isLikeArray: function(o){
		return o && o.length && o.length >= 0; 
	},
	isPaddy: function(o){
		return !!o.str;
	},
	isDOM: function(o){
		return !!o.nodeType;
	}
});
// 静态成员
paddy.extend({
	each: function(arr,callback){
		var i;
		if(paddy.isLikeArray(arr)){
			for(i = 0; i < arr.length; i++){
				if(callback.call(arr[i], i, arr[i]) === false) break;
			}
		}else {
			for(i in arr){
				if(callback.call(arr[i], i, arr[i]) === false) break;
			}
		}
	},
	trim: function(str){
		if(String.prototype.trim) return str.trim();
		else return str.replace(/^\s+|\s+$/, '');
	}
});
// 功能性函数
paddy.fn.extend({
	appendTo: function(arr){
		var i,
			doms = this.elements,
			domL = doms.length,
			arrL;
		arr = paddy(arr);
		arrL = arr.length;
		paddy.each(arr.elements, function(k, v){
			for(i = 0; i < domL; i++){
				this.appendChild(
					arrL === k ?
					doms[i] :
					doms[i].cloneNode(true));
			}
		});
	}
});
// html字符串解析
function parseHTML(html){
	var div = document.createElement('div'),
		arr = [];
	div.innerHTML = html;
	paddy.each(div.childNodes, function(k, v){
		arr[k] = v;
	});
	return arr;
}
// 解决IE中push.apply伪数组不能调用情况
var push = [].push;
try{
	push.apply(arr1, arr2);
}catch(e){
	push = {
		apply: function(arr1, arr2){
			var i = arr1.length,
				j = 0;
			while(arr1[i++] = arr2[j++]);
			return arr1.length = i - 1;
		},
		call: function(arr){
			var i = arr.length,
				j = 1;
			while(arr[i++] = arguments[j++]);
			return arr.length = i - 1;
		}
	}
}
// 搜索引擎
function select(selector){
	var r = /^(?:#([\w\-_]+)|\.([\w\-_]+)|([\w]+)|(\*))$/,
		m = r.exec(selector),
		arr = [],
		doms;
	if(m[1]){
		push.call(arr, document.getElementById(m[1]));
	}else if(m[2]){
		if(document.getElementsByClassName){
			push.apply(arr, document.getElementsByClassName(m[2]));
		}else {
			doms = document.getElementsByTagName('*');
			paddy.each(doms, function(k, v){
				if((' '+v.className+' ').indexOf(' '+m[2]+' ') != -1){
					arr.push(v);
				}
			});
		}
	}else {
		push.apply(arr, document.getElementsByTagName(m[3] ? m[3] : m[4]));
	}
	return arr;
}

return paddy;
})();