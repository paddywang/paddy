// PaddyWang
// 2016-04-16
// versions

(function(window, undefined){
var push = [].push,
	support = {},
	oldLoad;
try{
	push.apply(arr1, arr2);
}catch(e){
	push = {
		apply: function(arr1, arr2){
			var i = arr1.length,
				j = 0;
			while(arr1[i++] = arr2[j++]);
			return arr1.length = i - 1;
		},
		call: function(){
			var i = arguments[0].length;
				j = 1;
			while(arguments[0][i++] = arguments[j++]);
			return arguments[0].length = i - 1;
		}
	};
}
// 能力检测
support.getElementsByClassName = (function(){
	var dom,
		doms,
		ability = false;
	if(document.getElementsByClassName){
		dom = document.createElement('div');
		dom.innerHTML = '<p class="c">p</p>';
		doms = dom.getElementsByClassName('c');
		doms.length > 0 ? ability = true : ability = false;
	}
	return ability;
})();
support.firstElementChild = (function(){
	var ability = false;
	if(document.body.firstElementChild){
		ability = true;
	}
	return ability;
})();
function paddy(p){
	return new paddy.fn.init(p);
}
paddy.fn = paddy.prototype = {
	construcotr: paddy,
	selector: null,
	length: 0,
	init: function(p){
		if(paddy.isString(p)){
			if(!p) return paddy;
			if(p.charAt(0) === '<'){
				// html
				push.apply(this, paddy.parseHTML(p));
			}else {
				// selector
				push.apply(this, paddy.select(p));
				this.selector = p;
			}
		}else if(paddy.isPaddy(p)){
			return p;
		}else if(paddy.isLikeArray(p)){
			push.apply(this, p);
		}else if(paddy.isDom(p)){
			push.call(this, p);
		}else if(paddy.isFunction(p)){
			oldLoad = window.onload;
			if(typeof oldLoad != 'function'){
				window.onload = p;
			}else {
				oldLoad();
				p();
			}
		}
	},
	extend: function(obj){
		var k;
		for(k in obj){
			this[k] = obj[k];
		}
	}
};
paddy.fn.init.prototype = paddy.fn;
paddy.extend = paddy.fn.extend;
// 判断函数
paddy.extend({
	isString: function(str){
		return typeof str === 'string';
	},
	isNumber: function(num){
		return typeof num === 'number';
	},
	isBoolean: function(bool){
		return typeof bool === 'boolean';
	},
	isFunction: function(fn){
		return typeof fn === 'function';
	},
	isLikeArray: function(arr){
		return arr.length && arr.length >= 0; 
	},
	isPaddy: function(p){
		return 'selector' in p;
	},
	isDom: function(dom){
		return !!dom.nodeType;
	}
});
// each & trim
paddy.extend({
	each: function(arr, callback){
		var i;
		if(paddy.isLikeArray(arr)){
			for(i = 0; i < arr.length; i++){
				if(callback.call(arr[i], i, arr[i]) === false) break;
			}
		}else {
			for(i in arr){
				if(callback.call(arr[i], i, arr[i]) === false) break;
			}
		}
	},
	trim: function(str){
		if(String.prototype.trim) return str.trim();
		else return str.replace(/^\s+|\s+$/, '');
	}
});
// 字符串解析 & 搜索引擎
paddy.extend({
	parseHTML: function(html){
		var div = document.createElement('div'),
			arr = [],
			doms;
		div.innerHTML = html;
		doms = div.childNodes;
		paddy.each(doms, function(){
			arr.push(this);
		});
		return arr;
	},
	select: function(selector){
		var r = /^(?:#([\w\-_])+|\.([\w\-_]+)|([\w]+)|(\*))$/,
			m = r.exec(selector),
			arr = [],
			dom,
			doms;
		if(m){
			if(m[1]){
				dom = document.getElementById(m[1]);
				if(dom){
					push.call(arr, dom);
				}else arr = [];
			}else if(m[2]){
				if(support.getElementsByClassName){
					push.apply(arr, document.getElementsByClassName(m[2]));
				}else {
					doms = document.getElementsByTagName('*');
					paddy.each(doms, function(){
						if(' '+this.className+' '.indexOf(' '+m[2]+' ') != -1){
							push.call(arr, this);
						}
					});
				}
				
			}else {
				push.apply(arr, document.getElementsByTagName(!!m[3] ? 
																m[3] : 
																m[4]));
			}
		}
		return arr;
	}
});
// dom操作
paddy.extend({
	firstChild: function(dom){
		if(support.firstElementChild){
			return dom.firstElementChild;
		}else {
			return dom.firstChild;
		}
	}
});
paddy.fn.extend({
	// paddy(a).appendTo(b)
	// 将前面的a元素追加到指定的b元素里面
	appendTo: function(p){
		var doms = this;
		p = paddy(p);
		paddy.each(p, function(k1, v1){
			paddy.each(doms, function(k2, v2){
				v1.appendChild(v2.cloneNode(true));
			});
		});
	},
	// paddy(a).append(b)
	// 将后面指定的b元素追加到前面的a元素里面
	append: function(p){
		paddy(p).appendTo(this);
	},
	// paddy(a).propendTo(b)
	// 将前面的a元素追加到后面b元素的开始位置
	prependTo: function(p){
		var node,
			arr = [],
			l;
		p = paddy(p);
		l = p.length;
		paddy.each(this, function(k1, v1){
			paddy.each(p, function(k2, v2){
				node = l === k2 ? 
						v1 :
						v1.cloneNode(true);
				arr.push(node);
				this.insertBefore(node, paddy.firstChild(this));
			});
		});
		return paddy(arr);
	},
	// paddy(a).prepend(b)
	// 将指定的b元素添加到a元素的开始位置
	prepend: function(p){
		paddy(p).prependTo(this);
		return this;
	}
});
// 事件
/* paddy.fn.extend({
	click: function(event, callbakc){
		paddy.each(this, function(){
			this.addEventListener(event, callback);
		});
	}
 });*/
var = 'click, focus, blur, mouseover, mouseleave ';
paddy.each({
	paddy.fn[event] = function(event, callback){}
});
paddy.fn.extend({
	on: function(event, callback){
		paddy.each(this, function(){
			this.addEventListener(event, callback);
		});
		return this;
	},
	off: function(event, callback){
		paddy.each(this, function(){
			this.removeEventListener(event, callback);
		});
		return this;
	}
});

window.P = window.paddy = paddy;
})(window);