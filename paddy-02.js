// PaddyWang

(function(window, undefined){
var push = [].push;
try{
	push.apply(arr1, arr2);
}catch(e){
	push = {
		apply: function(arr1, arr2){
			var i = arr1.length,
				j = 0;
			while(arr1[i++] = arr2[j++]);
			return arr1.length = i - 1;
		},
		call: function(arr){
			var i = arr.length,
				j = 1;
			while(arr[i++] = arguments[j++]);
			return arr.length = i - 1;
		}
	};
}
function paddy(p){
	return new paddy.fn.init(p);
}
paddy.fn = paddy.prototype = {
	constructor: paddy,
	str: null,
	length: 0,
	init: function(p){
		if(!p) return;
		if(paddy.isString(p)){
			if(p.charAt(0) === '<'){
				// html
				push.apply(this, parseHTML(p));
			}else {
				// selector
				push.apply(this, select(p));
			}
		}
		this.str = 'paddy';
	},
	extend: function(obj){
		var k;
		for(k in obj){
			this[k] = obj[k];
		}
	}
};
paddy.fn.init.prototype = paddy.fn;
paddy.extend = paddy.fn.extend;

// 静态方法  判断函数
paddy.extend({
	isString: function(o){
		return typeof o === 'string';
	},
	isNumber: function(o){
		return typeof o === 'number';
	},
	isBoolean: function(o){
		return typeof o === 'boolean';
	},
	isFunction: function(o){
		return typeof o === 'function';
	},
	isLikeArray: function(o){
		return o && o.length && o.length >= 0;
	},
	isPaddy: function(o){
		return !!o.str;
	},
	isDOM: function(o){
		return !!o.nodeType;
	}
});
// 静态方法  常用方法
paddy.extend({
	each: function(arr, callback){
		var i;
		if(paddy.isLikeArray(arr)){
			for(i = 0; i < arr.length; i++){
				if(callback.call(arr[i], i, arr[i]) === false) break;
			}
		}else {
			for(i in arr){
				if(callback.call(arr[i], i, arr[i]) === false) break;
			}
		}
		return arr;
	},
	trim: function(str){
		if(String.prototype.trim) return str.trim();
		else return str.replace(/^\s+|\s+$/, '');
	},
	firstChild: function(dom){
		var doms = dom.childNodes,
			l = doms.length,
			i = 0;
		for(; i < l; i++){
			if(doms[i].nodeType === 1) return doms[i];
		}
	},
	lastChild: function(dom){
		var doms = dom.childNodes,
			l = doms.length,
			i = l;
		for(; i > 0; i--){
			if(doms[i] === 1) return doms[i];
		}
	}
});
// 功能方法
paddy.fn.extend({
	appendTo: function(p){
		var i,
			j,
			doms = this,
			domL = this.length,
			pL,
			arr,
			node;
		p = paddy(p);
		pL = p.length;
		for(i = 0; i < pL; i++){
			for(j = 0; j < domL; j++){
				node = i === pL - 1 ?
								doms[j] : 
								doms[j].cloneNode(true);
				arr.push(node);
				p[i].appendChild(node);
			}
		}
	},
	append: function(p){
		paddy(p).appendTo(this);
	},
	prependTo: function(p){
		var arr = [],
			node,
			pL;
		p = paddy(p);
		pL = p.length;
		paddy.each(this, function(k1, v1){
			paddy.each(p, function(k2, v2){
				node = k2 === pL - 1 ?
							v1 : 
							v1.cloneNode(true);
				arr.push(node);
				v2.insertBefore(node, paddy.firstChild(v2));
			});
		});
	},
	prepend: function(p){
		paddy(p).prependTo(this);
	},
	remove: function(){
		var arr = [];
		paddy.each(this, function(){
			arr.push(this);
			this.parentNode.removeChild(this);
		});
		return arr;
	}
});
// 解析字符串
function parseHTML(html){
	var div = document.createElement('div'),
		arr = [];
	div.innerHTML = html;
	paddy.each(div.childNodes, function(k, v){
		arr[k] = v;
	});
	return arr;
}
// 搜索引擎
function select(selector){
	var r = /^(?:#([\w\-_]+)|\.([\w\-_]+)|([\w]+)|(\*))$/,
		m = r.exec(selector),
		arr = [],
		doms;
	if(m[1]){
		push.call(arr, document.getElementById(m[1]));
	}else if(m[2]){
		if(document.getElementsByClassName){
			push.apply(arr, document.getElementsByClassName(m[2]));
		}else {
			doms = document.getElementsByTagName('*');
			paddy.each(doms, function(k, v){
				if((' '+v.className+' ').indexOf(' '+m[2]+' ') != -1){
					arr.push(v);
				}
			});
		}
	}else {
		push.apply(arr, document.getElementsByTagName(m[3] ? m[3] : m[4]));
	}
	return arr;
}
window.P = window.paddy = paddy;
})(window);